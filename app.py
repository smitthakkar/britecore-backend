from flask import Flask
from flask_restful import Api
from flask_mongoengine import MongoEngine
from flask_cors import CORS

from resources.risk import RisksResource
from resources.risk import RiskResource
from resources.risk_data import RiskDataResource


app = Flask(__name__)
api = Api(app)
db = MongoEngine()
CORS(app)

app.config.from_pyfile("settings.py")
db.init_app(app)

api.add_resource(RisksResource, "/api/risks")
api.add_resource(RiskResource, "/api/risk/<string:id>")
api.add_resource(RiskDataResource, "/api/risk/data/<string:risk_id>")

if __name__ == "__main__":
    app.run(debug=True)
