from unittest import TestCase
import json

from app import app
from models.risk import Risk
from models.data import RiskData
from models.risk import NumberFieldProps
from models.risk import TextFieldProps
from models.risk import RiskField


class RiskDataCRUDTestCase(TestCase):
    def setUp(self):
        self.client = app.test_client()
        risk_field = RiskField()
        risk_field.name = "FICO Score"
        risk_field.field_type = "number"
        risk_field.props = NumberFieldProps(
            min_value=300, max_value=850, allow_floating_point=False
        )
        risk_field.props.required = True
        risk_field2 = RiskField()
        risk_field2.name = "Profession"
        risk_field2.field_type = "text"
        risk_field2.props = TextFieldProps()
        risk_field2.props.required = False
        self.risk = Risk.objects.create(name="test", risk_fields=[risk_field, risk_field2])

    def test_create_risk_record_success(self):
        payload = {"form_data": {"FICO Score": 350, "Profession": ""}}
        response = self.client.post(
            f"/api/risk/data/{self.risk.id}",
            data=json.dumps(payload),
            content_type="application/json",
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(len(RiskData.objects()), 1)
        risk_data = RiskData.objects()[0]
        self.assertEqual(risk_data.data[0].field_name, "FICO Score")
        self.assertEqual(risk_data.data[0].value, "350")
        self.assertEqual(len(risk_data.data), 1) # Profession is empty so shouldn't be considered

    def test_create_risk_record_validation_error(self):
        payload = {"form_data": {"FICO Score": 3500}}
        response = self.client.post(
            f"/api/risk/data/{self.risk.id}",
            data=json.dumps(payload),
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)
        response = json.loads(response.data)
        self.assertEqual(
            response, {"errors": {"FICO Score": "Validation Error for FICO Score"}}
        )

    def test_create_risk_record_missing_attr_error(self):
        payload = {"form_data": {}}
        response = self.client.post(
            f"/api/risk/data/{self.risk.id}",
            data=json.dumps(payload),
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)
        response = json.loads(response.data)
        self.assertEqual(
            response,
            {"errors": {"FICO Score": "Empty or None value found for FICO Score"}},
        )

    def test_get_risk_record(self):
        # Calling the api to crete data quickly
        payload = {"form_data": {"FICO Score": 350}}
        self.client.post(
            f"/api/risk/data/{self.risk.id}",
            data=json.dumps(payload),
            content_type="application/json",
        )

        response = self.client.get(f"/api/risk/data/{self.risk.id}")
        self.assertEquals(200, response.status_code)

    def tearDown(self):
        RiskData.objects().delete()
        Risk.objects().delete()
