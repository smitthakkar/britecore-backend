from unittest import TestCase
import json

from app import app

from resources.risk import RiskResource
from models.risk import Risk
from models.risk import RiskField


class ValidateRiskFieldTestCase(TestCase):
    def test_validate_risk_fields_no_validation_error(self):
        """ _validate_risk_fields: Test _validate_risk_fields with correct data. It should
        return empty dict as a result of expected outcome. """
        # Given
        fields = [
            {
                "name": "FICO score",
                "type": "number",
                "props": {
                    "min_value": 300,
                    "max_value": 800,
                    "allow_floating_point": False,
                },
            },
            {"name": "Profession", "type": "text"},
        ]

        # When
        errors = RiskResource()._validate_risk_fields(fields)

        # Then
        self.assertFalse(errors)

    def test_validate_risk_fields_validation_error_invalid_property(self):
        """ _validate_risk_fields: Test _validate_risk_fields with invalid property for risk field.
        It should return error """
        # Given
        fields = [
            {
                "name": "FICO score",
                "type": "number",
                "props": {
                    "min_value": 300,
                    "max_value": 800,
                    "allow_floating_numbers": False,  # this is invalid
                },
            }
        ]

        # When
        errors = RiskResource()._validate_risk_fields(fields)

        # Then
        self.assertDictEqual(
            errors, {"FICO score": "allow_floating_numbers is invalid property"}
        )

    def test_validate_risk_fields_validation_error_invalid_type(self):
        """ _validate_risk_fields: Test _validate_risk_fields with invalid datatype for risk field.
        It should return error """
        # Given
        fields = [
            {
                "name": "FICO score",
                "type": "integer",  # this is invalid
                "props": {
                    "min_value": 300,
                    "max_value": 800,
                    "allow_floating_numbers": False,
                },
            }
        ]

        # When
        errors = RiskResource()._validate_risk_fields(fields)

        # Then
        self.assertDictEqual(
            errors, {"FICO score": "Invalid field type for FICO score"}
        )


class CRUDRiskTestCase(TestCase):
    def setUp(self):
        self.client = app.test_client()

    def test_create_risk_validation_error(self):
        # Given
        fields = [
            {
                "name": "FICO score",
                "type": "number",
                "props": {
                    "min_value": 300,
                    "max_value": 800,
                    "allow_floating_numbers": False,  # this is invalid
                },
            }
        ]

        payload = {"name": "lendingclub.com", "fields": fields}

        # When
        response = self.client.post(
            "/api/risks", data=json.dumps(payload), content_type="application/json"
        )

        # Then
        self.assertEqual(400, response.status_code)
        response = json.loads(response.data)
        self.assertDictEqual(
            {"errors": {"FICO score": "allow_floating_numbers is invalid property"}},
            response,
        )

    def test_create_risk_success(self):
        # Given
        fields = [
            {
                "name": "FICO score",
                "type": "number",
                "props": {
                    "min_value": 300,
                    "max_value": 800,
                    "allow_floating_point": False,
                },
            }
        ]

        payload = {"name": "lendingclub.com", "fields": fields}

        # When
        response = self.client.post(
            "/api/risks", data=json.dumps(payload), content_type="application/json"
        )

        # Then
        self.assertEqual(200, response.status_code)
        response = json.loads(response.data)
        self.assertIn("id", response)

    def test_update_risk_success(self):
        # Given
        risk = Risk.objects.create(name="test")
        fields = [
            {
                "name": "FICO score",
                "type": "number",
                "props": {
                    "min_value": 300,
                    "max_value": 800,
                    "allow_floating_point": False,
                },
            }
        ]

        payload = {"name": "lendingclub.com", "fields": fields}

        # When
        response = self.client.put(
            f"/api/risk/{risk.id}",
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Then
        self.assertEqual(200, response.status_code)
        updated_risk_object = Risk.objects.get(id=risk.id)
        self.assertEqual(len(updated_risk_object.risk_fields), 1)

    def test_update_risk_validation_error(self):
        # Given
        risk = Risk.objects.create(name="test")
        fields = [
            {
                "name": "FICO score",
                "type": "number",
                "props": {
                    "min_value": 300,
                    "max_value": 800,
                    "allow_floating_numbers": False,  # this is invalid
                },
            }
        ]

        payload = {"name": "lendingclub.com", "fields": fields}

        # When
        response = self.client.put(
            f"/api/risk/{risk.id}",
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Then
        self.assertEqual(400, response.status_code)
        response = json.loads(response.data)
        self.assertDictEqual(
            {"errors": {"FICO score": "allow_floating_numbers is invalid property"}},
            response,
        )

    def test_delete_risk(self):
        # Given
        risk = Risk.objects.create(name="test")

        # When
        response = self.client.delete(
            f"/api/risk/{risk.id}", content_type="application/json"
        )

        # Then
        self.assertEqual(200, response.status_code)
        self.assertEqual(0, len(Risk.objects(id=risk.id)))

    def test_get_one(self):
        # Given
        risk_field = RiskField()
        risk_field.name = "test"
        risk_field.field_type = "number"
        risk = Risk.objects.create(name="test", risk_fields=[risk_field])

        # When
        response = self.client.get(
            f"/api/risk/{risk.id}", content_type="application/json"
        )

        # Then
        self.assertEqual(200, response.status_code)
        response = json.loads(response.data)["response"]
        self.assertIn("name", response)
        self.assertIn("created_at", response)
        self.assertIn("risk_fields", response)

    def test_get_all(self):
        # Given
        risk_field = RiskField()
        risk_field.name = "test"
        risk_field.field_type = "number"
        Risk.objects.create(name="test", risk_fields=[risk_field])
        Risk.objects.create(name="test1", risk_fields=[risk_field])

        # When
        response = self.client.get("/api/risks", content_type="application/json")

        # Then
        self.assertEqual(200, response.status_code)
        self.assertEqual(2, len(Risk.objects()))

    def tearDown(self):
        Risk.objects().delete()
