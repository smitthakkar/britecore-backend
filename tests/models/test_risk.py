import unittest

from models.risk import DateFieldProps
from models.risk import TextFieldProps
from models.risk import EnumFieldProps
from models.risk import NumberFieldProps


class ValidatorsTestCase(unittest.TestCase):

    def test_enum_field_props_init_string_choices(self):
        enum_field = EnumFieldProps(choices='night,morning')
        self.assertListEqual(enum_field.choices, ['night', 'morning'])

    def test_enum_field_props_init_list_choices(self):
        enum_field = EnumFieldProps(choices=['night','morning'])
        self.assertListEqual(enum_field.choices, ['night', 'morning'])

    def test_date_field_validator_success(self):
        date_field = DateFieldProps()
        self.assertTrue(date_field.validate_input("1999/06/29"))

    def test_date_field_validator_invalid_value(self):
        date_field = DateFieldProps()
        self.assertFalse(date_field.validate_input("06/13/29"))

    def test_text_field_validator_success_without_max_length(self):
        string_field = TextFieldProps()
        self.assertTrue(string_field.validate_input("Test"))

    def test_text_field_validator_success_with_max_length(self):
        string_field = TextFieldProps(max_length=4)
        self.assertTrue(string_field.validate_input("Test"))

    def test_text_field_validator_invalid_value_with_max_length(self):
        string_field = TextFieldProps(max_length=4)
        self.assertFalse(string_field.validate_input("Testt"))

    def test_enum_field_validator_success(self):
        enum_field = EnumFieldProps(choices=["USA", "INDIA", "RUSSIA"])
        self.assertTrue(enum_field.validate_input("RUSSIA"))

    def test_enum_field_validator_invalid_value(self):
        enum_field = EnumFieldProps(choices=["USA", "INDIA"])
        self.assertFalse(enum_field.validate_input("RUSSIA"))

    def test_number_field_all_defaults_success(self):
        number_field = NumberFieldProps()
        self.assertTrue(number_field.validate_input(1))

    def test_number_field_all_defaults_invalid_value(self):
        number_field = NumberFieldProps()
        self.assertFalse(number_field.validate_input("it is a string"))

    def test_number_field_min_max_length_success(self):
        number_field = NumberFieldProps(min_value=3, max_value=5)
        self.assertTrue(number_field.validate_input(4))

    def test_number_field_min_max_length_invalid_value(self):
        number_field = NumberFieldProps(min_value=3, max_value=5)
        self.assertFalse(number_field.validate_input(7))

    def test_number_field_min_max_floating_point_true_length_success(self):
        number_field = NumberFieldProps(
            min_value=3, max_value=5, allow_floating_point=True
        )
        self.assertTrue(number_field.validate_input(3.3))

    def test_number_field_min_max_floating_point_false_length_success(self):
        number_field = NumberFieldProps(
            min_value=3, max_value=5, allow_floating_point=False
        )
        self.assertFalse(number_field.validate_input(3.3))
