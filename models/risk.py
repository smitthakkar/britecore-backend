""" Models for risks """

from datetime import datetime

from mongoengine import Document
from mongoengine import StringField
from mongoengine import DateTimeField
from mongoengine import ListField
from mongoengine import EmbeddedDocument
from mongoengine import GenericEmbeddedDocumentField
from mongoengine import EmbeddedDocumentListField
from mongoengine import IntField
from mongoengine import BooleanField


class DateFieldProps(EmbeddedDocument):
    input_format = StringField(default="%Y/%m/%d")
    output_format = StringField(default="%Y/%m/%d")
    required = BooleanField(default=False)

    def validate_input(self, value):
        try:
            datetime.strptime(value, self.input_format)
            return True
        except ValueError:
            return False


class TextFieldProps(EmbeddedDocument):
    max_length = StringField(default=None)
    required = BooleanField(default=False)

    def validate_input(self, value):
        if not self.max_length:
            return True

        if len(value) <= int(self.max_length):
            return True

        return False


class EnumFieldProps(EmbeddedDocument):
    choices = ListField()
    required = BooleanField(default=False)
    
    def __init__(self, *args, **kwargs):
        super(EnumFieldProps, self).__init__( *args, **kwargs)
        # HACK: Frontend currently sends comma seperated values
        if isinstance(self.choices, str):
            self.choices = self.choices.split(',')

    def validate_input(self, value):
        return value in self.choices


class NumberFieldProps(EmbeddedDocument):
    min_value = IntField(default=None)
    max_value = IntField(default=None)
    allow_floating_point = BooleanField(default=True)
    required = BooleanField(default=False)

    def validate_input(self, value):

        try:
            casted_value = float(value)
        except ValueError:
            return False

        is_in_range = True
        if self.min_value and self.max_value:
            is_in_range = casted_value >= self.min_value and casted_value <= self.max_value

        if not self.allow_floating_point:
            return isinstance(value, int) and is_in_range

        return is_in_range


FIELD_TYPE_PROPS_CLASS_MAPPINGS = {
    "date": DateFieldProps,
    "number": NumberFieldProps,
    "enum": EnumFieldProps,
    "text": TextFieldProps,
}


class RiskField(EmbeddedDocument):
    name = StringField(max_length=150, null=False)
    field_type = StringField(choices=FIELD_TYPE_PROPS_CLASS_MAPPINGS.keys(), null=False)
    props = GenericEmbeddedDocumentField()

    def as_dict(self):
        return {
            "name": self.name,
            "storeName": self.name,
            "type": self.field_type,
            "props": self.props.to_mongo().to_dict(),
        }

    def clean(self, *args, **kwargs):
        if not self.props:
            self.props = FIELD_TYPE_PROPS_CLASS_MAPPINGS[self.field_type]()
        super(RiskField, self).clean(*args, **kwargs)


class Risk(Document):
    name = StringField(max_length=150, null=False)
    created_at = DateTimeField(default=datetime.now)
    risk_fields = EmbeddedDocumentListField(RiskField)

    def as_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "created_at": str(self.created_at),
            "risk_fields": [field.as_dict() for field in self.risk_fields],
        }
