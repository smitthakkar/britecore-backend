from datetime import datetime

from mongoengine import Document
from mongoengine import ReferenceField
from mongoengine import DateTimeField
from mongoengine import StringField
from mongoengine import EmbeddedDocumentListField
from mongoengine import EmbeddedDocument

from models.risk import Risk


class Attributes(EmbeddedDocument):
    field_name = StringField()
    value = StringField()

    def clean(self, *args, **kwargs):
        self.value = str(self.value)
        super(Attributes, self).clean(*args, **kwargs)


class RiskData(Document):

    risk = ReferenceField(Risk)
    created_at = DateTimeField(default=datetime.now)
    data = EmbeddedDocumentListField(Attributes, default=[])

    def as_dict(self):
        return {
            "risk": str(self.risk.id),
            "created_at": str(self.created_at),
            "data": {attr.field_name: attr.value for attr in self.data},
        }
