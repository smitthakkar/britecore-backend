from envparse import env

MONGODB_SETTINGS = {
    "host": env("BRITE_CORE_DB_HOST", "127.0.0.1"),
}
