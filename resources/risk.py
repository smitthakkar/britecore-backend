from flask_restful import Resource
from flask_restful import reqparse

from models.risk import Risk
from models.risk import RiskField
from models.risk import FIELD_TYPE_PROPS_CLASS_MAPPINGS


INVALID_TYPE_ERROR = "Invalid field type for {}"
INVALID_PROPERTY = "{} is invalid property"


class RiskFieldsMixin:
    def _validate_risk_fields(self, risk_fields):
        errors = {}
        for field in risk_fields:
            if field["type"] not in FIELD_TYPE_PROPS_CLASS_MAPPINGS:
                errors[field["name"]] = INVALID_TYPE_ERROR.format(field["name"])
                continue

            for key in field.get("props", {}):
                if key not in FIELD_TYPE_PROPS_CLASS_MAPPINGS[field["type"]]._fields:
                    errors[field["name"]] = INVALID_PROPERTY.format(key)
        return errors

    def _create_risk_field_objects(self, fields):
        risk_field_objects = []
        for field in fields:
            props_class = FIELD_TYPE_PROPS_CLASS_MAPPINGS[field["type"]]
            props = props_class(**field["props"])
            risk_field = RiskField(name=field["name"], field_type=field["type"])
            risk_field.props = props
            risk_field_objects.append(risk_field)
        return risk_field_objects


class RisksResource(Resource, RiskFieldsMixin):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument(
            "name", required=True, help="name is required to create a risk"
        )
        parser.add_argument('required', default=False, type=bool)
        parser.add_argument(
            "fields",
            required=True,
            help="Please define the fields that are needed for the risk",
            type=dict,
            action="append",
        )
        arguments = parser.parse_args()
        field_validation_errors = self._validate_risk_fields(arguments["fields"])

        if field_validation_errors:
            return {"errors": field_validation_errors}, 400

        risk = Risk()
        risk.name = arguments["name"]
        risk.risk_fields = self._create_risk_field_objects(arguments["fields"])
        risk.save()
        return {"response": "Risk created", "id": str(risk.id)}

    def get(self):
        risks = Risk.objects()
        return {"response": [risk.as_dict() for risk in risks]}


class RiskResource(Resource, RiskFieldsMixin):
    def put(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument("name")
        parser.add_argument("fields", type=dict, action="append")
        arguments = parser.parse_args()

        risk = Risk.objects.get(id=id)
        for key, value in arguments.items():
            if key == "fields":
                continue
            risk[key] = value

        if "fields" in arguments:
            field_validation_errors = self._validate_risk_fields(arguments["fields"])

            if field_validation_errors:
                return {"errors": field_validation_errors}, 400

            risk.risk_fields = self._create_risk_field_objects(arguments["fields"])

        risk.save()
        return {"response": "Risk Updated"}

    def get(self, id):
        risk = Risk.objects.get(id=id)
        return {"response": risk.as_dict()}

    def delete(self, id):
        Risk.objects.get(id=id).delete()
        return {"response": "success"}
