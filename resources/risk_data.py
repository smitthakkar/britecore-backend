from flask_restful import Resource
from flask_restful import reqparse

from models.data import RiskData
from models.data import Attributes
from models.risk import Risk


FIELD_IS_REQUIRED = "Empty or None value found for {}"
VALIDATION_ERROR = "Validation Error for {}"


class RiskDataResource(Resource):
    def get(self, risk_id):
        return {
            "response": [
                risk_data.as_dict() for risk_data in RiskData.objects(risk=risk_id)
            ]
        }

    def _validate_form_data(self, risk, form_data):
        errors = {}
        for field in risk.risk_fields:
            if field.props.required:
                if field.name not in form_data or not form_data[field.name]:
                    errors[field.name] = FIELD_IS_REQUIRED.format(field.name)
                    continue
            else:
                if field.name not in form_data or not form_data[field.name]:
                    continue
            if not field.props.validate_input(form_data[field.name]):
                errors[field.name] = VALIDATION_ERROR.format(field.name)
        return errors

    def post(self, risk_id):
        parser = reqparse.RequestParser()
        parser.add_argument(
            "form_data", required=True, help="Please pass form data.", type=dict
        )

        risk = Risk.objects.get(id=risk_id)

        arguments = parser.parse_args()
        form_validation_errors = self._validate_form_data(risk, arguments["form_data"])

        if form_validation_errors:
            return {"errors": form_validation_errors}, 400

        risk_data = RiskData()
        risk_data.risk = risk
        for key, value in arguments["form_data"].items():
            if not value:
                continue
            risk_data.data.append(Attributes(field_name=key, value=value))
        risk_data.save()
        return {"response": "added"}
